# 0. 演示视频

配合视频演示食用更加哦~[视频演示点这里](https://gitee.com/bertramoon/dgut-autoreport-configure/raw/master/demo.mp4)

# 1. 配置

## 1.1. 登录Github

登录GitHub账号（没有的请自行[注册](https://github.com/signup)一下，很简单的，有电子邮箱即可注册。

## 1.2. Fork仓库
点击[https://github.com/Bertramoon/DGUT_Auto_Report](https://github.com/Bertramoon/DGUT_Auto_Report)，Fork该仓库（顺手Star一下是个好习惯~）

![Fork仓库](https://img-blog.csdnimg.cn/c2a1857acff145f087e57be8754e527e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)
![Fork成功](https://img-blog.csdnimg.cn/6d27a186ce53460691276be37f1fac8e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)


## 1.3. 配置Secrets

配置账号、密码等信息。账号变量是USERNAME，值是中央认证账号；密码变量是PASSWORD，值是中央认证密码

![点击secrets](https://gitee.com/bertramoon/img/raw/master/Auto_Report/%E8%AE%BE%E7%BD%AEsecrets.png)

![添加账号](https://img-blog.csdnimg.cn/706476d5d770472c8aa472383602cce6.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)

![添加密码](https://img-blog.csdnimg.cn/d2cfef4436bf444599fd85b80b9a2099.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 1.4. 自定义表单数据（非必要，一般按照默认即可）

如果想要自定义表单数据，请修改项目根目录下的`custom_data.json`文件。初始情况下，我们设置了两个字段：

- body_temperature: 体温，默认36.6
- health_situation: 健康情况，默认1，表示良好

如果你有需求，在该文件上继续添加或修改即可



# 2. 启动action

![启动action1](https://img-blog.csdnimg.cn/829ab2c53b0d49bf95daa68fe5f5d0b2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)![启动action2](https://img-blog.csdnimg.cn/9a243b9d9d4c420d9a1039872f87c64e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)![启动action3](https://img-blog.csdnimg.cn/addf702d9d6d408b90db1ee3a86e2c48.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)


# 3. 微信消息推送打卡成功通知配置


![Server微信通知1](https://img-blog.csdnimg.cn/b90ef9c126a3449698d1dd44cd14ae72.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)


![Server微信通知2](https://img-blog.csdnimg.cn/e2fdb07d211e4ed98dc7310669541d2d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)




如果你想要收到上面这样的通知的话，那么请跟着我的步骤做

## 3.1. 扫码登陆

首先，点击[https://sct.ftqq.com/login](https://sct.ftqq.com/login )，打开手机微信扫码登录

![手机扫码登录Server酱](https://img-blog.csdnimg.cn/e60a5a4a293645409509e2908614cb37.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 3.2. 扫码关注公众号

扫码并关注公众号后就会收到一条提示消息

![扫码并关注公众号](https://img-blog.csdnimg.cn/9ac941593a8a42f5846dc1f122635989.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)


然后回到电脑端页面，点击继续

![点击继续](https://img-blog.csdnimg.cn/7f9642c4eb3740f7b4e90b0b3ce3752b.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 3.3. 生成key

生成key，然后复制这个key

![生成并复制key](https://img-blog.csdnimg.cn/4dc6d74c83ab4d2c89cb4b5bb30aee17.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 3.4. 配置Secrets

![添加Server key](https://img-blog.csdnimg.cn/999b35d7ed2845b4a993e41b1f6f0f29.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAQmVydHJhbW9vbg==,size_20,color_FFFFFF,t_70,g_se,x_16)


**注意，1个key对应1个账号。对于一些特殊情况程序有特殊处理。以下是几种比较常见的情况和对应的配置方式，请参考**

1. 单号账号需要推送：直接按照上面步骤设置即可，即
    - USERNAME: 你的账号
    - PASSWORD: 你的密码
    - SERVER_KEY: "-K 你的Server_key" （注意：K是大写）
2. 多账号，每个账号都需要推送：必须为每个账号指定Server key，如果多个账号都是同个key的话，将这个key复制多次，例如，我们假设有3个账号，第1和第2个账号要推送到同一个微信，第3个账号要推送到另外一个微信
    - USERNAME: 账号1,账号2,账号3
    - PASSWORD: 密码1,密码2,密码3
    - SERVER_KEY: -K key1,key1,key2
3. 多账号，有些账号不需要推送：仍然必须为每个账号指定Server key，但是对于不需要推送的账号，我们只需要将key值设置为0即可。我们假设有4个账号，第1和第3个账号需要推送，第2和第4个账号不需要推送
    - USERNAME: 账号1,账号2,账号3,账号4
    - PASSWORD: 密码1,密码2,密码3,密码4
    - SERVER_KEY: -K key1,0,key3,0
        








